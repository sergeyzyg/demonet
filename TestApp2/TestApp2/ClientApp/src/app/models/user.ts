export class User {
  Id: string;
  Login: string;
  Password: string;
  CountryId: string;
  ProvinceId: string;

  public constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}
