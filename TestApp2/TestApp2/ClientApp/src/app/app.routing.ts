import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { AddressComponent } from './address/address.component';

const appRoutes: Routes = [
  { path: '', component: RegistrationComponent },
  { path: 'address', component: AddressComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
