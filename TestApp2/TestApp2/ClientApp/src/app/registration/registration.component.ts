import { Component, OnInit, VERSION } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user';
import { first } from 'rxjs/operators';
import { PasswordValidation } from './passwordvalidator';

@Component({ templateUrl: 'registration.component.html', providers: [UserService] })

export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  user: User = new User();
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern('(?=.*[0-9])(?=.*[A-Za-z]).*')]],
      confirmpassword: ['', [Validators.required, Validators.pattern('(?=.*[0-9])(?=.*[A-Za-z]).*')]],
      cb: [false, Validators.requiredTrue]
      },
    {
      validator: PasswordValidation.validate.bind(this)
    });
  }

  get f() { return this.registerForm.controls; }
  get form() { return this.registerForm; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.user = new User(this.registerForm.value);
    console.log(this.user.Login);
    this.loading = true;
    this.userService.registrationUser(this.user).pipe(first()).subscribe((data: User) => {
        this.loading = false;
        this.userService.saveCurrentUser(data);
        this.router.navigate(['/address']);
    },
      error => {
        console.log(error);
        this.loading = false;
      });
  }
}
