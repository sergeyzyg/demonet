import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddressService } from '../services/address.service';
import { UserService } from '../services/user.service';
import { Country } from '../models/country';
import { Province } from '../models/province';
import { User } from '../models/user';

@Component({ templateUrl: 'address.component.html', providers: [AddressService, UserService] })

export class AddressComponent implements OnInit {
  countries: Country[];
  provinces: Province[];
  currentUser: User;
  addressForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private countryService: AddressService,
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.loadData();
    this.addressForm = this.formBuilder.group({
      country: ['', Validators.required],
      province: ['', Validators.required],
    });
  }

  loadData() {
    this.currentUser = this.userService.getCurrentUser();
    this.countryService.getCountry()
      .subscribe((data: Country[]) => {
        this.countries = data
        console.log(this.countries);
      });
  }

  get f() { return this.addressForm.controls; }

  countrySelected(countryId: any) {
    this.countryService.getProvince(countryId)
      .subscribe((data: Province[]) => {
        this.provinces = data
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.addressForm.invalid) {
      return;
    }
    this.loading = true;
    this.currentUser.CountryId = this.addressForm.controls.country.value;
    this.currentUser.ProvinceId = this.addressForm.controls.province.value;
    this.userService.updateUser(this.currentUser).subscribe(data => {
          console.log(data);
          this.loading = false;
        },
        error => {
          console.log(error);
          this.loading = false;
        });
  }
}



