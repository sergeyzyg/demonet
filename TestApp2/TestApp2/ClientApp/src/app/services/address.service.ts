import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AddressService {

  private countryUrl = "/api/country";
  private provinceUrl = "/api/province";

  constructor(private http: HttpClient) { }

  getCountry() {
    return this.http.get(this.countryUrl);
  }

  getProvince(countryId) {
    return this.http.get(this.provinceUrl + "/" + countryId);
  }
}
