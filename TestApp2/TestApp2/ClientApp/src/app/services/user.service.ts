import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable()
export class UserService {
  private url = "/api/user";
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get(this.url);
  }

  registrationUser(user: User) {
    console.log(user);
    return this.http.post(this.url, user);
  }

  updateUser(user: User) {
    return this.http.put(this.url, user);
  }

  saveCurrentUser(user: User) {
    localStorage.setItem('currentUser', JSON.stringify(user))
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
