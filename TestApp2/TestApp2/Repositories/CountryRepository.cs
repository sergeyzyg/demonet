﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp2.Models;

namespace TestApp2.Repositories
{
    public class CountryRepository : IRepository<Country>
    {
        private ApplicationContext db;

        public CountryRepository(ApplicationContext context)
        {
            db = context;
            var list = GetItemsList().Result;
            if (list.Count() <= 0)
            {
                Create(new Country { Name = "Ukraine" });
                Create(new Country { Name = "Russia" });
                Save();
            }
        }

        public async Task<Country> GetCountryId(string name)
        {
            return await db.Countries.Where(c => c.Name.Equals(name)).FirstAsync();
        }

        public async Task<IEnumerable<Country>> GetItemsList()
        {
            return await db.Countries.ToListAsync();
        }

        public async Task<Country> GetItem(string id)
        {
            return await db.Countries.FindAsync(id);
        }

        public void Create(Country country)
        {
            db.Countries.Add(country);
        }

        public void Update(Country country)
        {
            db.Entry(country).State = EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }   
}
