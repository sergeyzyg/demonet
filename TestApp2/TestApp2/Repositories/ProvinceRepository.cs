﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp2.Models;

namespace TestApp2.Repositories
{
    public class ProvinceRepository : IRepositoryExt<Province>
    {
        private ApplicationContext db;

        public ProvinceRepository(ApplicationContext context)
        {
            db = context;
            var list = GetItemsList().Result;
            if (list.Count() <= 0)
            {
                var countryDB = new CountryRepository(context);
                Create(new Province { Name = "Kharkov", CountryId = countryDB.GetCountryId("Ukraine").Result.Id });
                Create(new Province { Name = "Moskow", CountryId = countryDB.GetCountryId("Russia").Result.Id });
                Save();
            }
        }

        public async Task<IEnumerable<Province>> GetItemsList()
        {
            return await db.Provinces.ToListAsync();
        }

        public async Task<IEnumerable<Province>> GetItemsList(string predicate)
        {
            return await db.Provinces.Where(p => p.CountryId.Equals(predicate)).ToListAsync();
        }

        public async Task<Province> GetItem(string id)
        {
            return await db.Provinces.FindAsync(id);
        }

        public void Create(Province province)
        {
            db.Provinces.Add(province);
        }

        public void Update(Province province)
        {
            db.Entry(province).State = EntityState.Modified;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

