﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp2.Repositories
{
    interface IRepository<T> : IDisposable
        where T : class
    {
        Task<IEnumerable<T>> GetItemsList();
        Task<T> GetItem(string id);
        void Create(T item);
        void Update(T item);
        void Save();
    }

    interface IRepositoryExt<T> : IRepository<T>
        where T : class
    {
        Task<IEnumerable<T>> GetItemsList(string predicate);
    }
}
