public class User
{
    public string Id { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public string CountryId { get; set; }
    public string ProvinceId { get; set; }
}