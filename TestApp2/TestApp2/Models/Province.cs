public class Province
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string CountryId { get; set; }
}