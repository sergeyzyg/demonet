using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestApp2.Models;
using TestApp2.Repositories;

namespace TestApp2.Controllers
{
    [Route("api/province")]
    public class ProvinceController : Controller
    {
        IRepositoryExt<Province> db;

        public ProvinceController(ApplicationContext context)
        {
            db = new ProvinceRepository(context);
        }

        [HttpGet]
        public IEnumerable<Province> Get()
        {
            return db.GetItemsList().Result;
        }

        [HttpGet("{countryId}")]
        public IEnumerable<Province> Get(string countryId)
        {
            return db.GetItemsList(countryId).Result;
        }
    }
}