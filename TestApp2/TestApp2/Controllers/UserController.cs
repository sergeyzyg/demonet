using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestApp2.Models;
using Microsoft.EntityFrameworkCore;

namespace TestApp2.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        ApplicationContext db;

        public UserController(ApplicationContext context)
        {
            db = context;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await db.Users.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]User user)
        {
           if (ModelState.IsValid)
           {
              db.Users.Add(user);
              await db.SaveChangesAsync();
              return Ok(user);
           }
           return BadRequest(ModelState);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync([FromBody] User user)
        {
            db.Users.Update(user);
            await db.SaveChangesAsync();
            return Ok(user);
        }
    }
}