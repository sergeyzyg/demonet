using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestApp2.Models;
using TestApp2.Repositories;

namespace TestApp2.Controllers
{
    [Route("api/country")]
    public class CountryController : Controller
    {
        IRepository<Country> db;

        public CountryController(ApplicationContext context)
        {
            db = new CountryRepository(context);            
        }

        [HttpGet]
        public IEnumerable<Country> Get()
        {
            return db.GetItemsList().Result;
        }
    }
}