﻿var nodes = new vis.DataSet([
    { id: 1, label: 'VAG' },
    { id: 2, label: 'Volkswagen' },
    { id: 3, label: 'Seat' },
    { id: 4, label: 'Škoda' },
    { id: 5, label: 'Bugatti' },
    { id: 6, label: 'Audi AG' },
    { id: 7, label: 'Audi' },
    { id: 8, label: 'Ducati' },
    { id: 9, label: 'Lamborghini' }
]);
var edges = new vis.DataSet([
    { from: 1, to: 2 },
    { from: 1, to: 3 },
    { from: 1, to: 4 },
    { from: 1, to: 5 },
    { from: 1, to: 6 },
    { from: 6, to: 7 },
    { from: 6, to: 8 },
    { from: 6, to: 9 },
]);
var data = {
    nodes: nodes,
    edges: edges
};


var options = {
    autoResize: true,
    height: '100%',
    width: '100%',
    clickToUse: false,
    physics: {
        enabled: false,
        minVelocity: 0.75
    },
    edges: {
        arrows: {
            to: {
                enabled: true
            }
        },
        smooth: {
            forceDirection: "none"
        }
    },
}

var container = document.getElementById('vis');

var network = new vis.Network(container, data, options);