﻿google.charts.load('current', { packages: ["orgchart"] });
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('string', 'Name');
  data.addColumn('string', 'ToolTip');
  data.addRows([
      ['VAG', '', ''],
      ['Volkswagen', 'VAG', ''],
      ['Seat', 'VAG', ''],
      ['Škoda', 'VAG', ''],
      ['Bugatti', 'VAG', ''],
      ['Audi AG', 'VAG', ''],
      ['Audi', 'Audi AG', ''],
      ['Ducati', 'Audi AG', ''],
      ['Lamborghini', 'Audi AG', ''],
  ]);

  var chart = new google.visualization.OrgChart(document.getElementById('google'));
  chart.draw(data, { allowHtml: true });
}