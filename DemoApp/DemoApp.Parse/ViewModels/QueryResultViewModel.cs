﻿using System;
using System.Collections.Generic;

namespace DemoApp.Parse.Models
{
    public class QueryResultViewModel
    {
        public bool Pagination { get; set; }
        public int Pages { get; set; }
        public int CurrentPage { get; set; }
        public List<ParseHtmlViewModel>models { get; set; }

        public QueryResultViewModel()
        {
            models = new List<ParseHtmlViewModel>();
        }
    }
}