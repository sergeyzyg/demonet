﻿using System;

namespace DemoApp.Parse.Models
{
    public class ParseHtmlViewModel
    {
        public string Title { get; set; }
        public string Date { get; set; }
        public string Price { get; set; }
        public string Text { get; set; }
    }
}