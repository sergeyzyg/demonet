﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using DemoApp.Parse.Models;
using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;


namespace DemoApp.Parse
{
    public class ContentManager
    {
        static string url = "https://premier.ua/?q=";

        public async Task<QueryResultViewModel> GetContentAsync(string searchString, string page)
        {
            HtmlWeb web = new HtmlWeb();
            HtmlDocument htmlDocument = await web.LoadFromWebAsync(GenerateUrl(searchString, page));
            HtmlNode htmlNode = htmlDocument.DocumentNode;
            QueryResultViewModel resultModel = new QueryResultViewModel();

            if (htmlNode.QuerySelectorAll(".pagins").Any())
            {
                HtmlNode containerBlock = htmlNode.QuerySelector(".container");
                int result = (Convert.ToInt32(Regex.Match(containerBlock.QuerySelector(".acenter").InnerText, @"\d+").Value));
                resultModel.Pages = result % 15 > 0 ? (result / 15) + 1 : result / 15;
                resultModel.Pagination = true;
                resultModel.CurrentPage = CheckPage(page) ? Convert.ToInt32(page) : 1;
            }

            HtmlNode node = htmlNode.QuerySelector("div[class=v-list]");
            IEnumerable<HtmlNode> items = node.QuerySelectorAll(".adv-item");
            foreach (HtmlNode item in items)
            {
                ParseHtmlViewModel parseModel = new ParseHtmlViewModel();
                parseModel.Title = item.QuerySelector(".adv-title").InnerText;
                parseModel.Price = item.QuerySelector(".adv-price").InnerText;
                parseModel.Date = item.QuerySelector("td>strong").InnerText;
                parseModel.Text = item.QuerySelector(".adv-item-text").InnerText;
                resultModel.models.Add(parseModel);
            }

            return resultModel;
        }

        private string GenerateUrl(string searchString, string page)
        {
            if (CheckPage(page))
            {
                return url + searchString + "&page=" + page;
            }
            else
            {
                return url + searchString;
            }
        }

        private bool CheckPage(string page)
        {
            return page != null && page.Count() > 0 ? true : false;
        }
    }
}
